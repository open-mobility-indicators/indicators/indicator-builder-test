FROM alpine

RUN apk add --no-cache bash

WORKDIR /data

COPY . .

ENTRYPOINT [ "bash" ]